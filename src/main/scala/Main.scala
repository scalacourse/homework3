trait Wealth {
	var money: Int
	var power: Int
}

trait GreatHouse {
	val name: String
	val wealth : Wealth
}

trait MakeWildFire {
	this: GreatHouse =>

	def makeWildFire() = this.wealth.power += 50
}

trait BorrowMoney {
	this: GreatHouse =>

	def borrowMoney() = this.wealth.money += 25
}

trait CallDragon {
	this: GreatHouse =>

	def callDragon() = this.wealth.power *= 2
}

case class Targaryen(name: String, override val wealth: Wealth)
	extends GreatHouse with MakeWildFire with CallDragon {
}

case class Lannisters(name: String, override val wealth: Wealth)
	extends GreatHouse with MakeWildFire with BorrowMoney {
}

case class GameOfThrones(first: GreatHouse, second: GreatHouse) {
	var round: Int = 1
	
	def nextRound(first: () => Unit)(second: () => Unit): GameOfThrones = {
		if (round > 0) {
			first()
			second()
			round += 1
		}

		this
	}
	
	def nextBattle(): GameOfThrones = {
		if (round > 0) {
			round += 1
		}

		if (first.wealth.power == second.wealth.power) {
			first.wealth.power /= 2
			first.wealth.money /= 2
			
			second.wealth.power /= 2
			second.wealth.money /= 2

			return this
		}

		if (first.wealth.power > second.wealth.power) {
			val deltaPower = first.wealth.power - second.wealth.power
			val deltaMoney = second.wealth.money / 2

			first.wealth.power -= deltaPower / 2
			second.wealth.power -= deltaPower

			first.wealth.money += deltaMoney / 2
			second.wealth.money -= deltaMoney
		} else {
			val deltaPower = second.wealth.power - first.wealth.power
			val deltaMoney = first.wealth.money / 2

			second.wealth.power -= deltaPower / 2
			first.wealth.power -= deltaPower

			second.wealth.money += deltaMoney / 2
			first.wealth.money -= deltaMoney
		}

		if (first.wealth.power <= 0 || first.wealth.money <= 0 ||
			second.wealth.power <= 0 || second.wealth.money <= 0) {
			round = 0 // the GoT has ended
		}

		return this
	}
}

object Main extends App {
	val tar = new Targaryen("Team A", new Wealth {
		var money = 100
		var power = 100
	});
	
	val lan = new Lannisters("Team B", new Wealth {
		var money = 100
		var power = 200
	});

	val got = new GameOfThrones(tar, lan)

	got
		.nextRound(tar.callDragon)(lan.makeWildFire)
		.nextRound(tar.makeWildFire)(lan.makeWildFire)
		.nextBattle()
		.nextRound(tar.makeWildFire)(lan.borrowMoney)
		.nextBattle()

	println(got.round)
	
	println(tar.name)
	println(tar.wealth.money)
	println(tar.wealth.power)

	println(lan.name)
	println(lan.wealth.money)
	println(lan.wealth.power)
}